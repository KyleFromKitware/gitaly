stages:
  - build
  - test
  - qa

default:
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:debian-${DEBIAN_VERSION}-ruby-${RUBY_VERSION}-golang-${GO_VERSION}-git-2.33-postgresql-11
  tags:
    - gitlab-org

variables:
  SAST_DISABLE_DIND: "true"
  SAST_DEFAULT_ANALYZERS: "gosec"
  DEBIAN_VERSION: "bullseye"
  # We use Gitaly's Git version by default.
  GIT_VERSION: "default"
  GO_VERSION: "1.17"
  RUBY_VERSION: "2.7"
  POSTGRES_VERSION: "12.6-alpine"
  PGBOUNCER_VERSION: "1.16.1"
  BUNDLE_PATH: "${CI_PROJECT_DIR}/.ruby"
  GOPATH: "${CI_PROJECT_DIR}/.go"
  # Run tests with an intercepted home directory so that we detect cases where
  # Gitaly picks up the gitconfig even though it ought not to.
  GITALY_TESTING_INTERCEPT_HOME: "YesPlease"

include:
  - template: Workflows/MergeRequest-Pipelines.gitlab-ci.yml
  - template: Security/License-Scanning.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/Secret-Detection.gitlab-ci.yml
  - project: 'gitlab-org/quality/pipeline-common'
    file:
      - '/ci/danger-review.yml'

.cache_deps:
  cache: &cache_deps_configuration
    key:
      files:
        - .gitlab-ci.yml
        - Makefile
      prefix: git-${GIT_VERSION}
    paths:
      - _build/deps
      - _build/tools
    policy: pull

.cache_gems:
  cache: &cache_gems_configuration
    key:
      files:
        - .gitlab-ci.yml
        - ruby/Gemfile.lock
      prefix: debian-${DEBIAN_VERSION}-ruby-${RUBY_VERSION}
    paths:
      - .ruby
    policy: pull

.cache_go:
  cache: &cache_go_configuration
    key:
      files:
        - .gitlab-ci.yml
        - go.sum
      prefix: go-${GO_VERSION}
    paths:
      - .go/pkg/mod
      - _build/cache
    policy: pull

.test_template: &test_definition
  needs: []
  stage: test
  cache:
    - *cache_deps_configuration
    - *cache_gems_configuration
    - *cache_go_configuration
  services:
    - postgres:${POSTGRES_VERSION}
  variables: &postgres_variables
    PGHOST: postgres
    PGPORT: 5432
    PGUSER: postgres
    POSTGRES_DB: praefect_test
    POSTGRES_HOST_AUTH_METHOD: trust
    TEST_REPORT: /tmp/go-tests-report.xml
    TEST_COVERAGE_DIR: /tmp/coverage
  before_script: &test_before_script
    - go version
    - while ! psql -h $PGHOST -U $PGUSER -c 'SELECT 1' > /dev/null; do echo "awaiting Postgres service to be ready..." && sleep 1 ; done && echo "Postgres service is ready!"
  artifacts:
    paths:
      - ruby/tmp/gitaly-rspec-test.log
    reports:
      junit: ${TEST_REPORT}
    when: on_failure
    expire_in: 1 week

danger-review:
  needs: []
  stage: build
  allow_failure: true
  variables:
    BUNDLE_GEMFILE: danger/Gemfile
  cache:
    key:
      files:
        - danger/Gemfile.lock
      prefix: debian-${DEBIAN_VERSION}-ruby-${RUBY_VERSION}
    paths:
      - .ruby
    policy: pull-push

build:
  needs: []
  stage: build
  cache:
    - <<: *cache_deps_configuration
      policy: pull-push
    - <<: *cache_gems_configuration
      policy: pull-push
    - <<: *cache_go_configuration
      policy: pull-push
  script:
    - go version
    - make -j$(nproc) build $(pwd)/_build/tools/protoc $(test "${GIT_VERSION}" = "default" && echo "build-bundled-git" || echo "git")
    - _support/test-boot . ${TEST_BOOT_ARGS}
  parallel:
    matrix:
      - GO_VERSION: [ "1.16", "1.17" ]
        TEST_BOOT_ARGS: "--bundled-git"
      - GIT_VERSION: "v2.33.0"

build:binaries:
  needs: []
  stage: build
  cache:
    - *cache_deps_configuration
    - *cache_go_configuration
  only:
    - tags
  script:
    # Just in case we start running CI builds on other architectures in future
    - go version
    - make -j$(nproc) build
    - cd _build && sha256sum bin/* | tee checksums.sha256.txt
  artifacts:
    paths:
    - _build/checksums.sha256.txt
    - _build/bin/
    name: "${CI_JOB_NAME}:go-${GO_VERSION}-git-${GIT_VERSION}"
    expire_in: 6 months
  parallel:
    matrix:
      - GO_VERSION: [ "1.16", "1.17" ]

test:
  <<: *test_definition
  script:
    # We need to prepare test dependencies as privileged user.
    - make -j$(nproc) build prepare-tests $(test "${GIT_VERSION}" = default && echo WITH_BUNDLED_GIT=YesPlease)
    # But the actual tests should run unprivileged. This assures that we pay
    # proper attention to permission bits and that we don't modify the source
    # directory.
    - setpriv --reuid=9999 --regid=9999 --clear-groups --no-new-privs make ${TARGET} SKIP_RSPEC_BUILD=YesPlease $(test "${GIT_VERSION}" = default && echo WITH_BUNDLED_GIT=YesPlease)
  parallel:
    matrix:
      # The following jobs all test with our default Git version, which is
      # using bundled Git binaries.
      - GO_VERSION: [ "1.16", "1.17" ]
        TARGET: test
      - TARGET: [ test-with-proxies, test-with-praefect, race-go ]
      # We also verify that things work as expected with a non-bundled Git
      # version matching our minimum required Git version.
      - TARGET: test
        GIT_VERSION: "v2.33.0"
      # Execute tests with our minimum required Postgres version, as well. If
      # the minimum version changes, please change this to the new minimum
      # version. Furthermore, please make sure to update the minimum required
      # version in `datastore.CheckPostgresVersion()`.
      - POSTGRES_VERSION: "11.14-alpine"
        TARGET: [ test, test-with-praefect ]

test:coverage:
  <<: *test_definition
  coverage: /^total:\t+\(statements\)\t+\d+\.\d+%$/
  script:
    # We need to explicitly build all prerequisites so that we can run tests unprivileged.
    - make -j$(nproc) build prepare-tests $(pwd)/_build/tools/gocover-cobertura
    - setpriv --reuid=9999 --regid=9999 --clear-groups --no-new-privs make cover SKIP_RSPEC_BUILD=YesPlease
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: ${TEST_COVERAGE_DIR}/cobertura.xml

test:pgbouncer:
  <<: *test_definition
  services:
    - postgres:${POSTGRES_VERSION}
    - name: bitnami/pgbouncer:${PGBOUNCER_VERSION}
      alias: pgbouncer
  variables:
    <<: *postgres_variables
    # The following variables are used by PgBouncer to connect to Postgres.
    POSTGRESQL_HOST: "${PGHOST}"
    # The image doesn't support setting `auth_user`, so we're cheating and use
    # "command line injection" here. In any case, `auth_user` is required so
    # that we can connect as a different user, but authenticate as the actual
    # PGUSER. We can fix this when
    # https://github.com/bitnami/bitnami-docker-pgbouncer/pull/22 lands.
    POSTGRESQL_PORT: "${PGPORT} auth_user=${PGUSER}"
    POSTGRESQL_USERNAME: "${PGUSER}"
    # These variables define how PgBouncer itself is configured
    PGBOUNCER_AUTH_TYPE: trust
    PGBOUNCER_DATABASE: "*"
    PGBOUNCER_IGNORE_STARTUP_PARAMETERS: extra_float_digits
    PGBOUNCER_POOL_MODE: transaction
    PGBOUNCER_MAX_DB_CONNECTIONS: 100
    # And these are finally used by Gitaly's tests.
    PGHOST_PGBOUNCER: pgbouncer
    PGPORT_PGBOUNCER: 6432
    # We need to enable per-build networking such that the PgBouncer service
    # can reach Postgres.
    FF_NETWORK_PER_BUILD: "true"
  before_script:
    - *test_before_script
    - while ! psql -h "${PGHOST_PGBOUNCER}" -p "${PGPORT_PGBOUNCER}" -U "${PGUSER}" -c 'SELECT 1' > /dev/null; do echo "awaiting PgBouncer service to be ready..." && sleep 1 ; done && echo "PgBouncer service is ready!"
  script:
    # We need to explicitly build all prerequisites so that we can run tests unprivileged.
    - make -j$(nproc) build prepare-tests
    - setpriv --reuid=9999 --regid=9999 --clear-groups --no-new-privs make test-with-praefect SKIP_RSPEC_BUILD=YesPlease

test:nightly:
  <<: *test_definition
  script:
    - go version
    - make -j$(nproc) build prepare-tests
    - setpriv --reuid=9999 --regid=9999 --clear-groups --no-new-privs make ${TARGET} SKIP_RSPEC_BUILD=YesPlease
  parallel:
    matrix:
      - GIT_VERSION: [ "master", "next" ]
        TARGET: [ test, test-with-proxies, test-with-praefect ]
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'

test:praefect_smoke:
  <<: *test_definition
  script:
    - make -j$(nproc)
    - ruby -rerb -e 'ERB.new(ARGF.read).run' _support/config.praefect.toml.ci-sql-test.erb > config.praefect.toml
    - ./_build/bin/praefect -config config.praefect.toml sql-ping
    - ./_build/bin/praefect -config config.praefect.toml sql-migrate

verify:
  needs: []
  stage: test
  cache:
    - *cache_deps_configuration
    - *cache_gems_configuration
    - *cache_go_configuration
  script:
    # Download the dependencies in case there was no cache hit, otherwise
    # golang-ci lint will timeout downloading them.
    - go mod download
    - make -j$(nproc) verify
  artifacts:
    paths:
      - _build/proto.diff
      - ruby/proto/gitaly/*
      - proto/go/gitalypb/*
    when: on_failure

dbschema:
  needs: []
  stage: test
  cache:
    - *cache_deps_configuration
    - *cache_gems_configuration
    - *cache_go_configuration
  services:
    # The database version we use must match the version of `pg_dump` we have
    # available in the build image.
    - postgres:11.13-alpine
  variables:
    <<: *postgres_variables
  before_script:
    - while ! psql -h $PGHOST -U $PGUSER -c 'SELECT 1' > /dev/null; do echo "awaiting Postgres service to be ready..." && sleep 1 ; done && echo "Postgres service is ready!"
  script:
    - make dump-database-schema no-changes
  artifacts:
    paths:
      - _support/praefect-schema.sql
    when: on_failure

gosec-sast:
  needs: []
  cache:
    - *cache_go_configuration
  variables:
    GOPATH: "/go"
  before_script:
    # Our pipeline places GOPATH to $CI_PROJECT_DIR/.go so it can be cached.
    # This causes gosec-sast to find the module cache and scan all the sources of
    # the dependencies as well. This makes the scan time grow massively. This is
    # avoided by this job moving the GOPATH outside of the project directory along
    # with the cached modules if they were successfully extracted.
    #
    # SAST_EXCLUDED_PATHS is not sufficient as it only filters out the results but
    # still performs the expensive scan.
    - if [ -d .go ]; then mv .go $GOPATH; fi
  rules:
    - if: $SAST_DISABLED
      when: never
    - if: $CI_COMMIT_BRANCH && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG

license_scanning:
  needs: []
  rules:
    - if: $LICENSE_SCANNING_DISABLED
      when: never
    - if: $CI_COMMIT_BRANCH && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
  variables:
    LICENSE_FINDER_CLI_OPTS: '--aggregate-paths=. ruby'

gemnasium-dependency_scanning:
  needs: []
  rules:
    - if: $DEPENDENCY_SCANNING_DISABLED
      when: never
    - if: $CI_COMMIT_BRANCH && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG

secret_detection:
  needs: []
  rules:
    - if: $SECRET_DETECTION_DISABLED
      when: never
    - if: $CI_COMMIT_BRANCH && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG

trigger-qa:
  needs: []
  stage: qa
  trigger:
    project: gitlab-org/build/omnibus-gitlab-mirror
  variables:
    ALTERNATIVE_SOURCES: "true"
    GITALY_SERVER_VERSION: $CI_COMMIT_SHA
    GITALY_SERVER_ALTERNATIVE_REPO: $CI_PROJECT_URL
    ee: "true"
  rules:
    - if: $CI_MERGE_REQUEST_SOURCE_BRANCH_SHA != ""
      when: manual
      allow_failure: true
      variables:
        # Downstream pipeline does not fetch the merged result SHA.
        # Fix: https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/6482
        GITALY_SERVER_VERSION: $CI_MERGE_REQUEST_SOURCE_BRANCH_SHA
    - when: manual
      allow_failure: true

qa:nightly-praefect-migration-test:
  needs: []
  stage: qa
  trigger:
    project: gitlab-org/quality/praefect-migration-testing
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
